import requests
import yaml
import dateutil.parser
import logging
import hashlib

from st2reactor.sensor.base import PollingSensor

#urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

class SDRANHelmIndexPollingSensor(PollingSensor):
    """
    Polls a Helm chart index file and looks for updates to the `created` field.

    XXX: PollingSensor.__init__ is broken in 3.7, constructor mismatch, so we do it
    ourselves.
    """
    def __init__(self, sensor_service, config=None, poll_interval=60):
        super(SDRANHelmIndexPollingSensor, self).__init__(
            sensor_service, config=config, poll_interval=poll_interval)
        self.logger = None
        self.session = None
        self.url = None
        self.verify = True
        self.entry = None
        self.key_name = "helmindex"

    def setup(self):
        self.logger = self.sensor_service.get_logger(name=self.__class__.__name__) or logging.getLogger()
        self.session = requests.session()
        self.url = self._config["sdran_chart_index_url"]
        self.entry = self._config["sdran_chart_entry"] or None
        self.verify = bool(self._config["sdran_chart_index_url_verify"])
        h = hashlib.sha256()
        h.update(self.url.encode("utf8"))
        self.key_name = "helmindex_" + h.hexdigest()
        if not self.verify:
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            self.session.verify = self.verify

    def _get_newest_one(self, entry_item):
        if not "created" in entry_item:
            return None
        return dateutil.parser.parse(entry_item["created"]).timestamp()

    def _get_newest(self, entry_items):
        newest = None
        for entry_item in entry_items:
            ts = self._get_newest_one(entry_item)
            if ts and (not newest or ts > newest):
                newest = ts
        return newest

    def poll(self):
        newest_change = None
        should_trigger = False
        last_change = None

        last_change = self.sensor_service.get_value(self.key_name)
        if last_change:
            last_change = float(last_change)

        r = self.session.get(self.url)
        y = yaml.load(r.content, yaml.Loader)

        if not isinstance(y, dict) or not "entries" in y or not isinstance(y["entries"], dict):
            self.logger.warning("malformed helm chart at %s", self.url)
            return

        entry = None
        if self.entry:
            if not self.entry in y["entries"]:
                self.logger.debug("entry %s not in chart %s", self.entry, self.url)
                return
            else:
                newest_change = self._get_newest( y["entries"][self.entry])
                entry = self.entry
        else:
            for (entry, entry_items) in y["entries"].items():
                ts = self._get_newest(entry_items)
                if ts and (not newest_change or ts > newest_change):
                    newest_change = ts
                    newest_entry = entry

        self.logger.debug("newest_change = %r, last_change = %r", newest_change, last_change)

        # If this is our first check, do not fire.
        if not last_change:
            self.sensor_service.set_value(self.key_name, newest_change)
            self.logger.debug("recorded initial last_change %r", newest_change)
            return

        if newest_change > last_change:
            self.logger.info("last_change updated to %r, triggering", newest_change)
            payload = dict(
                last_change=last_change, newest_change=newest_change,
                url=self.url, entry=newest_entry)
            self.sensor_service.dispatch(
                trigger="oran.sdranchartupdate", payload=payload)
            self.sensor_service.set_value(self.key_name, newest_change)

        return

    def cleanup(self):
        pass

    def add_trigger(self, trigger):
        pass

    def update_trigger(self, trigger):
        pass

    def remove_trigger(self, trigger):
        pass
