version: 1.0

description: "Given one parameterization (Kubernetes, OSC Near-RT RIC, RICtest), stand up and configure these components to run a test with RICtest."

input:
  - oran_node
  - rictest_node
  - username
  - manager_privkey
  - kubernetes_overrides
  - oscric_overrides
  - rictest_config

vars:
  - kubernetes_overrides_string: ""
  - oscric_overrides_string: ""
  - oscric_version: ""
  - ric_e2_addr: ""
  - ric_e2_port: 36422
  - rictest_agent_id: null
  - rictest_agent_ip: null
  - tcms_testrun: null
  - tcms_execution_completed: null
  - tcms_execution_noerrors: null
  - tcms_execution_allsetup: null
  - tcms_execution_allsubscriptions: null
  - tcms_execution_allindications: null
  - tcms_execution_allshutdown: null
  - files: <% dict() %>
  - e2stats: <% dict() %>
  - captures: <% dict() %>
  - config_data: <% dict() %>
  - config_metadata: <% dict() %>
  - start_rfc3389: <% now().format("%Y-%m-%dT%H:%M:%SZ") %>

output:
  - result: true

tasks:

  init_vars:
    action: core.noop
    next:
      - publish:
          - kubernetes_overrides_string: '{% for k,v in ctx()["kubernetes_overrides"].items() %} -e {{k}}={{v}}{% endfor %}'
          - oscric_overrides_string: '{% for k,v in ctx()["oscric_overrides"].items() %} -e {{k}}={{v}}{% endfor %}'
          - oscric_version: '{{ ctx()["oscric_overrides"]["oscric_version"] + "-powder" if ctx()["oscric_overrides"]["do_powder_oscric"] else "" }}'
        do:
          - setup_tcms

  setup_tcms:
    action: oran.rictest-ric-setup-tcms ric_name='OSC Near-RT RIC' ric_version='<% ctx(oscric_version) %>' ric_description="OSC Near-RT RIC" notes='Kubernetes overrides = {{ ctx()["kubernetes_overrides_string"] | trim }}\n\nRIC version = {{ ctx()["oscric_version"] | trim }}\n\nRIC overrides = {{ ctx()["oscric_overrides_string"] | trim }}' tags=<% list(ctx(oscric_version)) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_testrun: <% result()["output"]["tcms_testrun"] %>
          - tcms_execution_completed: <% result()["output"]["tcms_execution_completed"] %>
          - tcms_execution_noerrors: <% result()["output"]["tcms_execution_noerrors"] %>
          - tcms_execution_allsetup: <% result()["output"]["tcms_execution_allsetup"] %>
          - tcms_execution_allsubscriptions: <% result()["output"]["tcms_execution_allsubscriptions"] %>
          - tcms_execution_allindications: <% result()["output"]["tcms_execution_allindications"] %>
          - tcms_execution_allshutdown: <% result()["output"]["tcms_execution_allshutdown"] %>
        do:
          - redeploy_oscric
          #- redeploy_kubernetes

#  redeploy_kubernetes:
#    action: k8s_profile.redeploy host='<% ctx().oran_node %>' private_key='<% ctx().manager_privkey %>' username='<% ctx().username %>' overrides='{{ ctx()["kubernetes_overrides_string"] | trim }}'
#    next:
#      - when: <% succeeded() %>
#        do:
#          - redeploy_oscric

  redeploy_oscric:
    action: oran_profile.oscric.redeploy host='<% ctx().oran_node %>' private_key='<% ctx().manager_privkey %>' username='<% ctx().username %>' overrides='{{ ctx()["oscric_overrides_string"] | trim }}' force_reset=<% True %>
    next:
      - when: <% succeeded() %>
        publish:
          - ric_e2_addr: <% result()["output"]["ric_e2_addr"] %>
          - ric_e2_port: <% result()["output"]["ric_e2_port"] %>
        do:
          - find_rictest_agent

  find_rictest_agent:
    action: keysight_rictest.agent.find_first
    next:
      - when: <% succeeded() %>
        publish:
          - rictest_agent_id: <% result()["result"]["data"]["agent"]["id"] %>
          - rictest_agent_ip: <% result()["result"]["data"]["agent"]["mgmtInterface"]["IP"][0]["IP"] %>
        do:
          - configure_rictest_agent

  configure_rictest_agent:
    action: keysight_rictest_profile.agent.reconfigure agent_ip=<% ctx(rictest_agent_ip) %> host=<% ctx(rictest_node) %> test_mode="ric"
    next:
      - when: <% succeeded() %>
        do:
          - run_rictest

  run_rictest:
    action: keysight_rictest.workflow.test ric_e2_addr=<% ctx().ric_e2_addr %> ric_e2_port=<% ctx().ric_e2_port %> oenb_count=<% ctx().rictest_config["oenb_count"] %> ognb_count=<% ctx().rictest_config["ognb_count"] %> ue_count=<% ctx().rictest_config["ue_count"] %> sustain=<% ctx().rictest_config["sustain"] %> cleanup=true agent_id=<% ctx(rictest_agent_id) %>
    next:
      - when: <% succeeded() %>
        publish:
          - files: <% result()["output"]["files"] %>
          - e2stats: <% result()["output"]["e2stats"] %>
          - captures: <% result()["output"]["captures"] %>
          - config_data: <% result()["output"]["config_data"] %>
          - config_metadata: <% result()["output"]["config_metadata"] %>
        do:
          - get_oscric_e2_metrics
          - tcms_attach_files
          - tcms_attach_captures
          - tcms_attach_config_data
          - tcms_attach_config_metadata
          - tcms_update_completed

  get_oscric_e2_metrics:
    action: oran_profile.oscric.get_e2_metrics host='<% ctx().oran_node %>' private_key='<% ctx().manager_privkey %>' username='<% ctx().username %>' %>
    next:
      - when: <% succeeded() %>
        publish:
          - metrics: <% result()["output"]["metrics"] %>
        do:
          - tcms_update_allsetup
          - tcms_update_allsubscriptions
          - tcms_update_allindications

  tcms_attach_files:
    with: k, v in <% ctx(files).items().toList() %>
#      items: <% [ctx(files).keys()] %>
    action: kiwitcms.testrun.addattachment run=<% ctx(tcms_testrun) %> filename=<% item(k) %> content=<% item(v) %>

  tcms_attach_captures:
    with: k, v in <% ctx(captures).items().toList() %>
    action: kiwitcms.testrun.addattachment run=<% ctx(tcms_testrun) %> filename=<% item(k) %> content=<% item(v) %>

  tcms_attach_config_data:
    action: kiwitcms.testrun.addattachment run=<% ctx(tcms_testrun) %> filename="config_data.json" content='{{ ctx()["config_data"] | to_json_string }}'

  tcms_attach_config_metadata:
    action: kiwitcms.testrun.addattachment run=<% ctx(tcms_testrun) %> filename="config_metadata.json" content='{{ ctx()["config_metadata"] | to_json_string }}'

  tcms_update_completed:
    action: kiwitcms.testexecution.update execution=<% ctx(tcms_execution_completed) %> status="PASSED"
    next:
      - do:
          - tcms_update_noerrors

  tcms_update_noerrors:
    action: kiwitcms.testexecution.update execution=<% ctx(tcms_execution_noerrors) %> status='{{ "PASSED" if (ctx()["e2stats"]["e2errors"] + ctx()["e2stats"]["e2abort"] + ctx()["e2stats"]["e2oob"]) == 0 else "FAILED" }}'
    next:
      - do:
          - tcms_update_allshutdown

  tcms_update_allshutdown:
    action: kiwitcms.testexecution.update execution=<% ctx(tcms_execution_allshutdown) %> status='{{ "PASSED" if (ctx()["e2stats"]["e2associations"] == ctx()["e2stats"]["e2shutdown"]) else "FAILED" }}'

  tcms_update_allsetup:
    action: kiwitcms.testexecution.update execution=<% ctx(tcms_execution_allsetup) %> status='{{ "PASSED" if (ctx()["metrics"]["SetupRequestMsgs"] == ctx()["metrics"]["SetupResponseMsgs"] and ctx()["metrics"]["SetupRequestMsgs"] == (ctx().rictest_config["ognb_count"]+ctx().rictest_config["oenb_count"])) else "FAILED" }}'

  tcms_update_allsubscriptions:
    #'{{ "PASSED" if (ctx()["e2subscriptionrequest"] <= ctx()["e2subscriptionresponse"]) else "FAILED" }}'
    action: kiwitcms.testexecution.update execution=<% ctx(tcms_execution_allsubscriptions) %> status='WAIVED'

  tcms_update_allindications:
    #'{{ "PASSED" if (ctx()["e2indication"] == ctx()["e2stats"]["e2indications"]) else "FAILED" }}'
    action: kiwitcms.testexecution.update execution=<% ctx(tcms_execution_allindications) %> status='WAIVED'
