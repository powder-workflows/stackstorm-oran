version: 1.0

description: "Configure TCMS metadata for SD-RAN/RICtest test cases and create a run and test executions."

input:
  - sdran_version
  - kubernetes_overrides
  - sdran_overrides

vars:
  - tcms_classification: null
  - tcms_product: null
  - tcms_version: null
  - tcms_plantype: null
  - tcms_testplan: null
  - tcms_build: null
  - tcms_testcase_completed: null
  - tcms_testcase_noerrors: null
  - tcms_testcase_allsetup: null
  - tcms_testcase_allsubscriptions: null
  - tcms_testcase_allindications: null
  - tcms_testcase_allshutdown: null
  - tcms_testrun: null
  - tcms_execution_completed: null
  - tcms_execution_noerrors: null
  - tcms_execution_allsetup: null
  - tcms_execution_allsubscriptions: null
  - tcms_execution_allindications: null
  - tcms_execution_allshutdown: null

output:
  - tcms_testrun: <% ctx(tcms_testrun) %>
  - tcms_execution_completed: <% ctx(tcms_execution_completed) %>
  - tcms_execution_noerrors: <% ctx(tcms_execution_noerrors) %>
  - tcms_execution_allsetup: <% ctx(tcms_execution_allsetup) %>
  - tcms_execution_allsubscriptions: <% ctx(tcms_execution_allsubscriptions) %>
  - tcms_execution_allindications: <% ctx(tcms_execution_allindications) %>
  - tcms_execution_allshutdown: <% ctx(tcms_execution_allshutdown) %>

tasks:

  tcms_classification_get:
    action: kiwitcms.classification.get name="O-RAN"
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_classification: <% result().result["id"] %>
        do:
          - tcms_product_ensure

  tcms_product_ensure:
    action: kiwitcms.product.ensure name="SD-RAN" description="SD-RAN is developing a near-real-time RIC (nRT-RIC) and a set of exemplar xApps for controlling the RAN. This RIC is cloud-native and builds on several of ONF’s well established platforms including the ONOS SDN Controller. The architecture for the SD-RAN nRT-RIC will leverage the O-RAN architecture and vision." classification=<% ctx(tcms_classification) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_product: <% result().result["id"] %>
        do:
          - tcms_version_ensure

  tcms_version_ensure:
    action: kiwitcms.version.ensure product=<% ctx(tcms_product) %> value=<% ctx(sdran_version) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_version: <% result().result["id"] %>
        do:
          - tcms_ensure_plantype

  tcms_ensure_plantype:
    action: kiwitcms.plantype.ensure name="Integration"
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_plantype: <% result().result["id"] %>
        do:
          - tcms_ensure_testplan

  tcms_ensure_testplan:
    action: kiwitcms.testplan.ensure name="SD-RAN RICtest" text="SD-RAN RICtest integration tests for version <% ctx(sdran_version) %>" type=<% ctx(tcms_plantype) %> product=<% ctx(tcms_product) %> product_version=<% ctx(tcms_version) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_testplan: <% result().result["id"] %>
        do:
          - tcms_ensure_build

  tcms_ensure_build:
    action: kiwitcms.build.ensure name=<% ctx(sdran_version) %> version=<% ctx(tcms_version) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_build: <% result().result["id"] %>
        do:
          - tcms_ensure_testcase_completed

  tcms_ensure_testcase_completed:
    action: kiwitcms.testcase.ensure summary="RICtest session completed" text="RICtest session completed" testplan=<% ctx(tcms_testplan) %> product=<% ctx(tcms_product) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_testcase_completed: <% result().result["id"] %>
        do:
          - tcms_ensure_testcase_noerrors

  tcms_ensure_testcase_noerrors:
    action: kiwitcms.testcase.ensure summary="RICtest reported no E2 errors" text="RICtest reported no E2 errors" testplan=<% ctx(tcms_testplan) %> product=<% ctx(tcms_product) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_testcase_noerrors: <% result().result["id"] %>
        do:
          - tcms_ensure_testcase_allsetup

  tcms_ensure_testcase_allsetup:
    action: kiwitcms.testcase.ensure summary="All expected E2setup procedures succeeded" text="All expected E2setup procedures succeeded" testplan=<% ctx(tcms_testplan) %> product=<% ctx(tcms_product) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_testcase_allsetup: <% result().result["id"] %>
        do:
          - tcms_ensure_testcase_allsubscriptions

  tcms_ensure_testcase_allsubscriptions:
    action: kiwitcms.testcase.ensure summary="All expected E2subscription procedures succeeded" text="All expected E2subscription procedures succeeded" testplan=<% ctx(tcms_testplan) %> product=<% ctx(tcms_product) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_testcase_allsubscriptions: <% result().result["id"] %>
        do:
          - tcms_ensure_testcase_allindications

  tcms_ensure_testcase_allindications:
    action: kiwitcms.testcase.ensure summary="All sent E2indication messages received" text="All sent E2indication messages received" testplan=<% ctx(tcms_testplan) %> product=<% ctx(tcms_product) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_testcase_allindications: <% result().result["id"] %>
        do:
          - tcms_ensure_testcase_allshutdown

  tcms_ensure_testcase_allshutdown:
    action: kiwitcms.testcase.ensure summary="All E2 associations shutdown" text="All E2 associations shutdown" testplan=<% ctx(tcms_testplan) %> product=<% ctx(tcms_product) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_testcase_allshutdown: <% result().result["id"] %>
        do:
          - tcms_ensure_testrun

  tcms_ensure_testrun:
    action: kiwitcms.testrun.create summary="Testing SD-RAN (version=<% ctx(sdran_version) %>, overrides=[<% ctx(sdran_overrides) %>] kubernetes_overrides=[<% ctx(kubernetes_overrides) %>])" build=<% ctx(tcms_build) %> plan=<% ctx(tcms_testplan) %>
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_testrun: <% result().result["id"] %>
        do:
          - tcms_addexec_completed

  tcms_addexec_completed:
    action: kiwitcms.testrun.addcase run=<% ctx(tcms_testrun) %> case=<% ctx(tcms_testcase_completed) %> status="RUNNING"
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_execution_completed: <% result().result["id"] %>
        do:
          - tcms_addexec_noerrors

  tcms_addexec_noerrors:
    action: kiwitcms.testrun.addcase run=<% ctx(tcms_testrun) %> case=<% ctx(tcms_testcase_noerrors) %> status="RUNNING"
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_execution_noerrors: <% result().result["id"] %>
        do:
          - tcms_addexec_allsetup

  tcms_addexec_allsetup:
    action: kiwitcms.testrun.addcase run=<% ctx(tcms_testrun) %> case=<% ctx(tcms_testcase_allsetup) %> status="RUNNING"
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_execution_allsetup: <% result().result["id"] %>
        do:
          - tcms_addexec_allsubscriptions

  tcms_addexec_allsubscriptions:
    action: kiwitcms.testrun.addcase run=<% ctx(tcms_testrun) %> case=<% ctx(tcms_testcase_allsubscriptions) %> status="WAIVED"
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_execution_allsubscriptions: <% result().result["id"] %>
        do:
          - tcms_addexec_allindications

  tcms_addexec_allindications:
    action: kiwitcms.testrun.addcase run=<% ctx(tcms_testrun) %> case=<% ctx(tcms_testcase_allindications) %> status="RUNNING"
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_execution_allindications: <% result().result["id"] %>
        do:
          - tcms_addexec_allshutdown

  tcms_addexec_allshutdown:
    action: kiwitcms.testrun.addcase run=<% ctx(tcms_testrun) %> case=<% ctx(tcms_testcase_allshutdown) %> status="RUNNING"
    next:
      - when: <% succeeded() %>
        publish:
          - tcms_execution_allshutdown: <% result().result["id"] %>
