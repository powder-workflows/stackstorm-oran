version: 1.0

description: "A more complex SD-RAN testing workflow: iterates over Kubernetes and SD-RAN configurations and applies Keysight RICtest to test."

input:
  - oran_expt_name
  - oran_expt_proj
  - rictest_expt_name
  - rictest_expt_proj
  - shared_vlan
  - shared_vlan_expt_name
  - shared_vlan_expt_proj
  - rictest_endpoint
  - kubernetes_overrides
  - sdran_configs
  - rictest_configs
  - manager_privkey
  - manager_pubkey
  - proj
  - expt_name_prefix
  - username

vars:
  - oran_node: null
  - oran_expt_status: null
  - rictest_node: null
  - rictest_expt_status: null
  - parameter_space: []

output:
  - result: true

tasks:

  create_parameter_space:
    action: emulab.util.itertools.product iterables=<% list(ctx().kubernetes_overrides,ctx().sdran_configs,ctx().rictest_configs) %>
    next:
      - publish:
          - parameter_space: <% result()["result"]["product"] %>
        do: log_parameter_space

  log_parameter_space:
    action: core.echo message='{{ ctx()["parameter_space"] | string }}'
    next:
      - do:
          - maybe_start_oran_expt
          - maybe_start_rictest_expt

  maybe_start_oran_expt:
    action: core.noop
    next:
      - when: <% not ctx().oran_expt_name %>
        publish:
          - oran_expt_name: '<% ctx().expt_name_prefix + "-oran" %>'
          - oran_node: node-0.<% ctx().expt_name_prefix + "-oran" %>.<% ctx().proj %>.emulab.net
        do:
          - start_oran_expt
      - when: <% ctx().oran_expt_name %>
        publish:
          - oran_node: node-0.<% ctx().oran_expt_name %>.<% ctx().proj %>.emulab.net
        do:
          - have_expts

  maybe_start_rictest_expt:
    action: core.noop
    next:
      - when: <% not ctx().rictest_expt_name %>
        publish:
          - rictest_expt_name: '<% ctx().expt_name_prefix + "-rictest" %>'
          - rictest_node: node-0.<% ctx().expt_name_prefix + "-rictest" %>.<% ctx().proj %>.emulab.net
          - shared_vlan: '<% ctx().expt_name_prefix + "-rt-shvlan" %>'
        do:
          - start_rictest_expt
      - when: <% ctx().rictest_expt_name %>
        publish:
          - rictest_node: node-0.<% ctx().rictest_expt_name %>.<% ctx().proj %>.emulab.net
        do:
          - have_expts

  start_oran_expt:
    action: emulab.experiment.create profile="emulab-ops,oran-ansible" name='<% ctx(oran_expt_name) %>' proj='<% ctx().proj %>' wait_for_status=True sshpubkey='<% ctx().manager_pubkey %>' bindings=<% dict(sslCertType=>'letsencrypt',disableAnsiblePlaybookAutomation=>"True",kubeFeatureGates=>'[SCTPSupport=true,EphemeralContainers=true]',kubeEnableMultus=>"True",createConnectableSharedVlan=>"True",sharedVlanAddress=>"10.254.254.1/255.255.255.0") %>
    next:
      - when: <% succeeded() %>
        publish:
          - oran_expt_status: <% result().result %>
          - oran_node: node-0.<% ctx().expt_name_prefix + '-oran.' + ctx().proj + '.emulab.net' %>
        do:
          - have_expts

  start_rictest_expt:
    action: emulab.experiment.create name="<% ctx(rictest_expt_name) %>" proj="<% ctx().proj %>" profile="emulab-ops,dijon-git" wait_for_status=True sshpubkey='<% ctx().manager_pubkey %>' bindings=<% dict(createSharedVlan=>"True",sharedVlanAddress=>"10.254.254.2/255.255.255.0",connectSharedVlan=>ctx(shared_vlan)) %>
    next:
      - when: <% succeeded() %>
        publish:
          - oran_rictest_status: <% result().result %>
          - rictest_node: node-0.<% ctx().expt_name_prefix + '-rictest.' + ctx().proj + '.emulab.net' %>
        do:
          - have_expts

  have_expts:
    # Note that we are creating or using two experiments, so we wait for the
    # two tasks to reach us, not `all` (which would be four, since we're
    # branching above).
    join: 2
    action: core.noop
    next:
      - do: maybe_connect_experiments

  maybe_connect_experiments:
    action: core.noop
    next:
      - when: <% ctx(oran_expt_status) and not ctx(shared_vlan_expt_name) %>
        do:
          - connect_oran_experiment_to_rictest
      - when: <% ctx(oran_expt_status) and ctx(shared_vlan_expt_name) %>
        do:
          - connect_oran_experiment_to_vlan
      - when: <% not ctx(oran_expt_status) %>
        do:
          - sdran_rictest_iteration

  connect_oran_experiment_to_rictest:
    action: emulab.experiment.connect experiment='<% ctx().proj %>,<% ctx(oran_expt_name) %>' lan="shared-vlan" target_experiment='<% ctx(rictest_expt_proj) %>,<% ctx(rictest_expt_name) %>' target_shared_vlan=<% ctx(shared_vlan) %>
    next:
      - when: <% succeeded() %>
        do:
          - sdran_rictest_iteration

  connect_oran_experiment_to_vlan:
    action: emulab.experiment.connect experiment='<% ctx().proj %>,<% ctx(oran_expt_name) %>' lan="shared-vlan" target_experiment='<% ctx(shared_vlan_expt_proj) %>,<% ctx(shared_vlan_expt_name) %>' target_shared_vlan=<% ctx(shared_vlan) %>
    next:
      - when: <% succeeded() %>
        do:
          - sdran_rictest_iteration

  sdran_rictest_iteration:
    with:
      items: <% ctx().parameter_space %>
      concurrency: 1
    action: oran.sdran-rictest-one oran_node=<% ctx(oran_node) %> kubernetes_overrides=<% item()[0] %> sdran_overrides=<% item()[1] %> rictest_config=<% item()[2] %> rictest_node=<% ctx(rictest_node) %>
    next:
      - when: <% succeeded() and ctx(oran_expt_status) %>
        do: terminate_oran_experiment
      - when: <% succeeded() and ctx(rictest_expt_status) %>
        do: terminate_rictest_experiment

  terminate_oran_experiment:
    action: emulab.experiment.terminate name='<% ctx(oran_expt_name) %>' proj='<% ctx().proj %>'

  terminate_rictest_experiment:
    action: emulab.experiment.terminate name='<% ctx(rictest_expt_name) %>' proj='<% ctx().proj %>'
    
