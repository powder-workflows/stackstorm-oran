---
name: sdran-rictest
pack: oran
description: "A more complex SD-RAN testing workflow: iterates over Kubernetes and SD-RAN configurations and applies Keysight RICtest to test."
runner_type: orquesta
entry_point: workflows/sdran-rictest.yaml
enabled: true
parameters:
  oran_expt_name:
    description: "The name of an existing POWDER O-RAN experiment."
    type: string
  oran_expt_proj:
    description: "The project containing the existing POWDER O-RAN experiment."
    type: string
    default: "{{ config_context.proj }}"
  rictest_expt_name:
    description: "The name of an existing RICtest experiment."
    type: string
  rictest_expt_proj:
    description: "The project containing the existing RICtest experiment."
    type: string
    default: "{{ config_context.proj }}"
  shared_vlan:
    description: "The name of an existing experiment's shared vlan."
    type: string
  shared_vlan_expt_name:
    description: "The name of an existing experiment that owns the shared vlan."
    type: string
  shared_vlan_expt_proj:
    description: "The name of an existing experiment's project that owns the shared vlan."
    type: string
    default: "{{ config_context.proj }}"
  rictest_endpoint:
    description: "The RICtest API endpoint of an existing RICtest experiment on POWDER."
    type: string
    default: "https://10.254.254.2"
  kubernetes_overrides:
    description: "A list of dicts of Kubernetes overrides to be passed to the Ansible playbook that (re)deploys Kubernetes."
    type: array
    required: true
    default:
      - kube_network_plugin: calico
        kube_pods_subnet: "10.233.0.0/16"
        kube_service_addresses: "10.96.0.0/12"
      - kube_network_plugin: flannel
        kube_pods_subnet: "10.233.0.0/16"
        kube_service_addresses: "10.96.0.0/12"
  sdran_configs:
    description: "An array of dicts of SD-RAN overrides to be passed to the Ansible playbook that (re)deploys SD-RAN."
    type: array
    required: true
    default:
      - sdran_version: 1.4.3
        do_powder_sdran: true
  rictest_configs:
    description: "An array of dicts containing parameters that override keysight_rictest.config.create parameters."
    type: array
    required: true
    default:
      - oenb_count: 1
        ognb_count: 1
        ue_count: 50
        sustain: 10
      - oenb_count: 5
        ognb_count: 5
        ue_count: 200
        sustain: 10
  proj:
    description: "The project in which to create the experiment"
    type: string
    required: true
    default: "{{ config_context.proj }}"
  username:
    description: "The username to use to invoke automation actions on experiments via ssh."
    required: true
    type: string
    default: "{{ config_context.username }}"
  expt_name_prefix:
    description: "A short prefix, serving as a namespace, to prepend along with `-` to experiments this workflow creates."
    required: true
    type: string
    default: "wf{{ ((range(1,10) | random) * (range(1,10) | random) * (range(1,10) | random)) - 1 }}"
  manager_privkey:
    description: "An SSH private key in PEM format"
    type: "string"
    default: "{{ config_context.manager_privkey }}"
    required: true
  manager_pubkey:
    description: "An SSH public key for authorized_keys"
    type: "string"
    default: "{{ config_context.manager_pubkey }}"
    required: true
